import * as fs from "fs";



import chromeWebstoreUpload from "chrome-webstore-upload"
// credentials from Chrome web store api

const REFRESH_TOKEN = process.env.REFRESH_TOKEN
const EXTENSION_ID = process.env.EXTENSION_ID
const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET

const zipFileName = 'dist/ext.zip'

const webstore = chromeWebstoreUpload({
	extensionId: EXTENSION_ID,
	clientId: CLIENT_ID,
	clientSecret: CLIENT_SECRET,
	refreshToken: REFRESH_TOKEN,
})


const uploadZip = () => {
	const extensionPackage = fs.createReadStream(`./${zipFileName}`)
	webstore.uploadExisting(extensionPackage).then(_ => {
		console.log('package uploaded')

		webstore.publish().then(_ => {
			console.log('new version published, awaiting for approval')
		}).catch(e => {
			console.log('error while publishing', e)
			process.exit(1)
		})
	}).catch(e => {
		console.log('error while zipping', e)
		process.exit(1);
	})
}

uploadZip()
